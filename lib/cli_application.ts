import * as minimist from "minimist";
import {parse} from "./parse";

export type Validation = RegExp | (() => void);

export type Parser = (input: string) => any;

export type ActionFunction = (args: { [key: string]: any }, options: { [key: string]: any }) => void;

export class Argument {
    constructor(public name: string, public help?: string, public test?: Validation) {

    }
}


export class Command {
    constructor(public _name: string, public help_text: string) {

    }

    args: Argument[] = [];
    options: Option[] = [];
    addArgument(arg: Argument) {
        this.args.push(arg);
    }

    addOption(option: Option) {
        this.options.push(option);
    }

    public action: ActionFunction;
    addAction(action: ActionFunction) {
        this.action = action;
    }



    get name() { return this._name }
    set name(v: string) { this._name = v }
}


export class Option {
    constructor(public _name: string, public help?: string, parser?: Parser) {

    }
}



interface CLIApplicationInterface {
    lastCommand: Command
    args: string[]
}

export class CLIApplication {
    private _version: string;
    version(version: string) {
        this._version = version;
        return this;
    }

    private _bin: string;
    bin(name: string) {
        this._bin = name;
    }

    protected lastCommand: Command;
    protected commands: Command[] = [];

    private _args: string[];

    protected get args() {
        return this._args || process.argv;
    }

    protected set args(value: string[]) {
        this._args = value;
    }

    command(name: string, help_text?: string) {
        this.onNewCommand();

        this.lastCommand = new Command(name, help_text)
        this.commands.push(this.lastCommand);
        return this;
    }

    private onNewCommand() {
        this.lastCommand = null;
    }

    argument(name: string, help?: string, validation?: Validation) {
        let arg = new Argument(name, help, validation);
        this.lastCommand.addArgument(arg);
        return this;
    }

    option(name: string, help?: string, parser?: (option: string) => any) {
        let option = new Option(name, help, parser);
        this.lastCommand.addOption(option);
        return this;
    }

    action(action_function: ActionFunction) {
        this.lastCommand.addAction(action_function);
        this.onNewCommand();
        return this;
    }

    parse(args?: string[]) {
        if (args)
            this._args = args;
            
        const m_args = minimist(this.args);
        let application = this;


        parse(application,m_args);

    }

}