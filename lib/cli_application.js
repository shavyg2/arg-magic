"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var minimist = require("minimist");
var parse_1 = require("./parse");
var Argument = (function () {
    function Argument(name, help, test) {
        this.name = name;
        this.help = help;
        this.test = test;
    }
    return Argument;
}());
exports.Argument = Argument;
var Command = (function () {
    function Command(_name, help_text) {
        this._name = _name;
        this.help_text = help_text;
        this.args = [];
        this.options = [];
    }
    Command.prototype.addArgument = function (arg) {
        this.args.push(arg);
    };
    Command.prototype.addOption = function (option) {
        this.options.push(option);
    };
    Command.prototype.addAction = function (action) {
        this.action = action;
    };
    Object.defineProperty(Command.prototype, "name", {
        get: function () { return this._name; },
        set: function (v) { this._name = v; },
        enumerable: true,
        configurable: true
    });
    return Command;
}());
exports.Command = Command;
var Option = (function () {
    function Option(_name, help, parser) {
        this._name = _name;
        this.help = help;
    }
    return Option;
}());
exports.Option = Option;
var CLIApplication = (function () {
    function CLIApplication() {
        this.commands = [];
    }
    CLIApplication.prototype.version = function (version) {
        this._version = version;
        return this;
    };
    CLIApplication.prototype.bin = function (name) {
        this._bin = name;
    };
    Object.defineProperty(CLIApplication.prototype, "args", {
        get: function () {
            return this._args || process.argv;
        },
        set: function (value) {
            this._args = value;
        },
        enumerable: true,
        configurable: true
    });
    CLIApplication.prototype.command = function (name, help_text) {
        this.onNewCommand();
        this.lastCommand = new Command(name, help_text);
        this.commands.push(this.lastCommand);
        return this;
    };
    CLIApplication.prototype.onNewCommand = function () {
        this.lastCommand = null;
    };
    CLIApplication.prototype.argument = function (name, help, validation) {
        var arg = new Argument(name, help, validation);
        this.lastCommand.addArgument(arg);
        return this;
    };
    CLIApplication.prototype.option = function (name, help, parser) {
        var option = new Option(name, help, parser);
        this.lastCommand.addOption(option);
        return this;
    };
    CLIApplication.prototype.action = function (action_function) {
        this.lastCommand.addAction(action_function);
        this.onNewCommand();
        return this;
    };
    CLIApplication.prototype.parse = function (args) {
        if (args)
            this._args = args;
        var m_args = minimist(this.args);
        var application = this;
        parse_1.parse(application, m_args);
    };
    return CLIApplication;
}());
exports.CLIApplication = CLIApplication;
